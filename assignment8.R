setwd('C:/Users/Darryl/Documents')
f <- file.choose()

install.packages("svmlin")
library(svmlin)

install.packages("e1071")
library(e1071)

install.packages("openNLP")
library(openNLP)

install.packages("SnowballC")
library(SnowballC)

install.packages("tm")
library(tm)

install.packages("topicmodels")
library(topicmodels)

install.packages("lda")
library(lda)

install.packages("wordcloud")
library(wordcloud)


data.reuters = read.csv(file="reutersCSV.csv",header=T,sep=",")
columns.needed <- c("pid", "fileName", "purpose",
                    "topic.earn","topic.acq","topic.money.fx","topic.grain","topic.crude",
                    "topic.trade","topic.interest","topic.ship","topic.wheat","topic.corn",
                    "doc.title", "doc.text")
data.reuters <- data.reuters[,columns.needed]

topic.zero <- c()
#clean columns that have no hits
for(i in 1:ncol(data.reuters)){
  if(is.numeric(data.reuters[,i])){
    if(sum(data.reuters[,i]) == 0){
      topic.zero <- c(topic.zero, i)
    }
  }
  
}
data.reuters <- data.reuters[,-topic.zero]
rm(topic.zero)
#clean rows that have no topics
no.topics <- c()
for(i in 1:nrow(data.reuters)){
    if(sum(data.reuters[i,4:13]) == 0 || data.reuters$doc.text==""){
      no.topics <- c(no.topics, i)
    }
}
data.reuters <- data.reuters[-no.topics,]


corpus <- data.reuters$doc.text
corpus <-  Corpus(VectorSource(corpus))
corpus <- tm_map(corpus, stripWhitespace)
corpus <- tm_map(corpus, content_transformer(tolower))
corpus <- tm_map(corpus, removeWords, c(stopwords("SMART"), "reuter", stopwords("english")))
corpus <- tm_map(corpus, removeNumbers)
corpus <- tm_map(corpus, stemDocument)


empty.doc <- c()
for(i in 1:length(corpus)){
  if(corpus[[i]][[1]] == ""){
    empty.doc <- c(empty.doc, i)
  }
}
data.reuters <- data.reuters[-empty.doc,]
corpus <- corpus[-empty.doc]

dtm<- DocumentTermMatrix(corpus,  control=list(minDocdata.Freq=5))
tdm <- TermDocumentMatrix(corpus, control=list(minDocFreq=5))
#dtm<- DocumentTermMatrix(corpus, list(dictionary=c("earn", "acquisitions", "money-fx", "grain", "crude", "trade", "interest",
 #                        "ship", "wheat", "corn")))
dtm2 <- removeSparseTerms(dtm, 0.98)
dtm.tf.idf.weighted <- weightTfIdf(dtm)

dtm.frame <- as.data.frame(inspect(dtm))
dtm.matrix <- as.matrix(dtm)

v<- apply(dtm.matrix, 2, sum)
v<- sort(v, decreasing=TRUE)
rowTotals <- c(1:length(corpus))

for(i in 1:dtm$nrow){
  rowTotals[i] <- sum(dtm[1,])
}

topics.list <- topics(lda.vem, k=10)
for(i in 1:length(topics.list)){
    if(topics.list >= 10){
      cat( names[i], ':\t', sum(data.reuters[,i]), '\n')
    }
}


sent_token_annotator <- Maxent_Sent_Token_Annotator()
word_token_annotator <- Maxent_Word_Token_Annotator()
pos_tag_annotator <- Maxent_POS_Tag_Annotator()

pos.tag.result <- list()
for(i in 1:length(corpus)){  
  pos.tag.result[[i]] <- annotate(corpus[[i]], list(sent_token_annotator, 
                                                    word_token_annotator, 
                                                    pos_tag_annotator))
}



#data cleaning the text

#pid, filename, purpose, story, doc.title, doc.text, dominant.topic
# reuters.set <- matrix(data = 0,nrow=nrow(data.reuters), ncol = 7)
# colnames(accuracies) <- c("pid", "filename", "purpose", "doc.title", "doc.text", "dominant.topic")
# 
# for(i in 1:nrow(data.reuters)){
#   reuters.set[i,1] <- data.reuters[i,1]
#   reuters.set[i,2] <- data.reuters[i,2]
#   reuters.set[i,3] <- data.reuters[i,3]
#   
#   if(is.numeric(data.reuters[,i])){
#     cat( names[i], ':\t', sum(data.reuters[ ,i]), '\n')
#   }
# }

  lda.vem <- LDA(dtm, method="VEM", k = 10)
  lda.gibbs <- LDA(dtm, method="Gibbs", k = 10)
  train_posterior <- posterior(lda.gibbs, dtm)

svmModel <- svm(topics(lda.gibbs, 1) ~ ., data = lda.gibbs)

freq <- sort(colSums(as.matrix(dtm)), decreasing=TRUE)

library(wordcloud)
set.seed(123)
wordcloud(names(freq), freq, min.freq=1200, colors=brewer.pal(8, "Dark2"))


#building the training data
document.topic <- topics(lda.gibbs, 1)
  #assigning classes randomly if the document has multiple classes ticked.
document.classes <- c()
for(i in 1:nrow(data.reuters)){
  if(sum(data.reuters[i,4:13]) > 1){
    num <-  which(data.reuters[i,4:13]==1)
    class <- sample(1:length(num), 1)    
  } else {
    class <- which(data.reuters[i,4:13]==1)
  }
  document.classes <- c(document.classes, names(data.reuters)[class+3])
}

dtm.f <- as.data.frame(inspect(dtm))

#accurracies
accuracies <- matrix(data = 0,nrow=10, ncol = 3)
colnames(accuracies) <- c("SVM", "Random Forest", "Naive Bayes")
#10-folds cross validation
fold.size <- nrow(data.reuters) / 10

preprocess.corpus <- function(corpus){
  corpus <-  Corpus(VectorSource(corpus))
  corpus <- tm_map(corpus, stripWhitespace)
  corpus <- tm_map(corpus, content_transformer(tolower))
  corpus <- tm_map(corpus, removeWords, c(stopwords("SMART"), "reuter", stopwords("english")))
  corpus <- tm_map(corpus, removeNumbers)
  corpus <- tm_map(corpus, stemDocument)
  return(corpus)
}

for(i in 1:10){
  
  training.data.rows <- sample(1:nrow(data.reuters), fold.size, replace=FALSE, prob=NULL)
  training.data <- data.reuters[training.data.rows,]
  test.data <- data.reuters[-training.data.rows,]
  
  training.corpus <- preprocess.corpus(training.data)
  testing.corpus <- preprocess.corpus(testing.data)
  
  training.dtm<- DocumentTermMatrix(training.corpus)
  testing.dtm<- DocumentTermMatrix(testing.corpus)
  
  lda <- LDA(dtm, k = 10, method = "Gibbs")
  
  train.posterior<-posterior(LDA_train,training.dtm)
  test.posterior<-posterior(LDA_train,testing.dtm)
  
  train.data.frame<- as.data.frame(as.matrix(train.posterior$topics))
  test.data.frame<- as.data.frame(as.matrix(test.posterior$topics))  
  
  svmModel <- svm(training.data$type ~ ., data = training.data)
  nbModel <- naiveBayes( training.data$type ~., data= training.data)
  
  svmModel.predicted <- predict(svmModel, test.data)
  nbModel.predicted <- predict(nbModel, test.data)
  
  svm.confusion.matrix <- table( pred = svmModel.predicted, true = test.data[, 12])
  nb.confusion.matrix <- table( pred = nbModel.predicted, true = test.data[, 12])
  
  svm.prf <- prfMatrix(svm.confusion.matrix)
  nb.prf <- prfMatrix(nb.confusion.matrix)
  svm.acc <- accuracy(svm.confusion.matrix, fold.size)
  nb.acc <- accuracy(nb.confusion.matrix, fold.size)
  

  accuracies[ i , 1 ] = svm.acc[1,1]
  accuracies[ i , 2 ] = nb.acc[1,1]
  
  cat('Fold ', i, '\n')
  cat('-------------------------------------------------------------------------------------\n')
  cat('SVM ')
  print(t(svm.prf))
  cat('\n')
  print(t(svm.acc))
  cat('-------------------------------------------------------------------------------------\n')
  cat('Naive Bayes ')
  print(t(nb.prf))
  cat('\n')
  print(t(nb.acc))
  cat('\n')
  cat('<------------------------------------------------------------------------------------->\n')
}
multi.class <- c()
for(i in 1:nrow(data.reuters)){
  if(sum(data.reuters[i,4:13]) >1 ){
    multi.class <- c(multi.class, i)
  }
}